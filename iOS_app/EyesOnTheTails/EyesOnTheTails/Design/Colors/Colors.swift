//
//  Colors.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 22.04.2024..
//

import UIKit

struct Colors {
    static let tailBlack = UIColor(red: 33.0/255.0, green: 36.0/255.0, blue: 38.0/255.0, alpha: 1)
    static let tailDarkGreen = UIColor(red: 43.0/255.0, green: 50.0/255.0, blue: 46.0/255.0, alpha: 1)
    static let tailBeige = UIColor(red: 214.0/255.0, green: 203.0/255.0, blue: 189.0/255.0, alpha: 1)
    static let tailDarkBeige = UIColor(red: 138.0/255.0, green: 127.0/255.0, blue: 112.0/255.0, alpha: 1)
    static let tailWhite = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1)
}
