//
//  MapViewModel.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 25.04.2024..
//

import UIKit
import Combine
import FirebaseFirestore
import MapKit

class MapViewModel {
    
    let database: Firestore
    let dbPublisher: PassthroughSubject<Horse, Never>
    let horses: [Int: String]
    let pastureCoordinates: [CLLocationCoordinate2D]
    
    init() {
        self.database = Firestore.firestore()
        self.dbPublisher = PassthroughSubject<Horse, Never>()
        self.horses = [
            1: "Riđa",
            2: "Bijeli pastuh"
        ]
        
        self.pastureCoordinates = [
            CLLocationCoordinate2D(latitude: 45.53707390953316, longitude: 18.671161815015303),
            CLLocationCoordinate2D(latitude: 45.531707952579154, longitude: 18.671054526657553),
            CLLocationCoordinate2D(latitude: 45.53167788975343, longitude: 18.670646830898107),
            CLLocationCoordinate2D(latitude: 45.53121191389997, longitude: 18.67038933883951),
            CLLocationCoordinate2D(latitude: 45.531061598284964, longitude: 18.6644455638202),
            CLLocationCoordinate2D(latitude: 45.534759245798796, longitude: 18.664638682864148),
            CLLocationCoordinate2D(latitude: 45.53516507035421, longitude: 18.666248008230387),
            CLLocationCoordinate2D(latitude: 45.53561598309206, longitude: 18.668071910312126),
            CLLocationCoordinate2D(latitude: 45.53633743595223, longitude: 18.67023913513866)
        ]
    }
    
    func fetchFirebase() {
        database.collection("horses").addSnapshotListener { (snapshot, error) in
                guard let snapshot = snapshot else {
                    return
                }
                
                snapshot.documentChanges.forEach { diff in
                    if (diff.type == .added || diff.type == .modified || diff.type == .removed) {
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: diff.document.data())
                            let horseInfo = try JSONDecoder().decode(Horse.self, from: jsonData)
                            self.dbPublisher.send(horseInfo)
                        } catch {
                            print("Error decoding data: \(FetchError.unknown)")
                        }
                    }
                }
            }
    }
    
    func encodeHorseName(id: Int) -> String {
        return self.horses[id] ?? "Nepoznati konj"
    }
}

enum FetchError: Error {
    case unknown
}
