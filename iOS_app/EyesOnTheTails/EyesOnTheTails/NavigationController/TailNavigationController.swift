//
//  TailNavigationController.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 24.04.2024..
//

import UIKit

class TailNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    func setupUI() {
        navigationBar.tintColor = Colors.tailBeige
    }
}
