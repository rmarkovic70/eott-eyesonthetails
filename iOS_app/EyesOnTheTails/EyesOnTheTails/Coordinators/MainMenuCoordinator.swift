//
//  MainMenuCoordinator.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 21.04.2024..
//

import UIKit

class MainMenuCoordinator: BaseCoordinator {
    
    let navigationController: TailNavigationController
    
    init(navigationController: TailNavigationController) {
        self.navigationController = navigationController
    }
    
    func start() -> UIViewController {
        let mainMenuVC = MainMenuViewController { [weak self] in
            self?.startMapVC()
        } gotoCredits: { [weak self] in
            self?.startCreditsVC()
        }
        return mainMenuVC
    }
    
    func startMapVC() {
        let mapVC = MapViewController()
        navigationController.pushViewController(mapVC, animated: true)
    }
    
    func startCreditsVC() {
        let creditsVC = CreditsViewController()
        navigationController.pushViewController(creditsVC, animated: true)
    }
}
