//
//  MainCoordinator.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 21.04.2024..
//

import UIKit

class MainCoordinator: BaseCoordinator {
    
    let navigationController: TailNavigationController
    let window: UIWindow
    let mainMenuCoordinator: MainMenuCoordinator
    
    init(window: UIWindow, navigationController: TailNavigationController) {
        self.window = window
        self.navigationController = navigationController
        self.mainMenuCoordinator = MainMenuCoordinator(navigationController: navigationController)
    }
    
    override func start() {
        window.rootViewController = navigationController
        navigationController.setViewControllers([mainMenuCoordinator.start()], animated: false)
    }
}
