//
//  BaseCoordinator.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 21.04.2024..
//

import Foundation

class BaseCoordinator: NSObject {
    
    override init() {}
    
    func start() {}
    
}
