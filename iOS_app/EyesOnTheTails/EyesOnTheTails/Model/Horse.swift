//
//  Horse.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 26.04.2024..
//

import UIKit

struct Horse: Codable {
    
    var horseId: Int
    var geoLatitude: Double
    var geoLongitude: Double
//    var isRaining: Bool
    
}
