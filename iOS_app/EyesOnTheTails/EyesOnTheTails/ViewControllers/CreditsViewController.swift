//
//  CreditsViewController.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 26.04.2024..
//

import UIKit

class CreditsViewController: UIViewController {

    private lazy var aboutLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Teko-Light", size: 25)
        label.textColor = Colors.tailBeige
        label.textAlignment = .left
        label.numberOfLines = 0
        label.text = "This is iOS project designed for horse GPS tracking."
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        addSubviews()
    }
    
    private func setupUI() {
        view.backgroundColor = Colors.tailDarkGreen
    }
    
    private func addSubviews() {
        view.addSubview(aboutLabel)
        constraint()
    }
    
    private func constraint() {
        aboutLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 25).isActive = true
        aboutLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 25).isActive = true
        aboutLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -25).isActive = true
        aboutLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
    }
}
