//
//  MapViewController.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 21.04.2024..
//

import UIKit
import MapKit
import Combine

class MapViewController: UIViewController {

    let mapViewModel: MapViewModel
    private var bag: Set<AnyCancellable>
    
    init() {
        self.mapViewModel = MapViewModel()
        self.bag = Set<AnyCancellable>()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var mapView: MKMapView = {
        let map = MKMapView()
        map.overrideUserInterfaceStyle = .light
        map.mapType = .hybridFlyover
        map.addOverlay(MKPolygon(coordinates: mapViewModel.pastureCoordinates, count: mapViewModel.pastureCoordinates.count))
        map.delegate = self
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
    }()
    
    private lazy var mapAnnotation: MKPointAnnotation = {
        let annotation = MKPointAnnotation()
        return annotation
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        mapViewModel.fetchFirebase()
        addSubviews()
    }

    private func bind() {
        mapViewModel.dbPublisher
            .receive(on: RunLoop.main)
            .sink { horseData in
                self.mapReload(horseData)
            }
            .store(in: &bag)
    }
    
    private func mapReload(_ horseData: Horse) {
        let newCoordinate = CLLocationCoordinate2D(latitude: horseData.geoLatitude, longitude: horseData.geoLongitude)
        mapAnnotation.coordinate = newCoordinate
        mapAnnotation.title = mapViewModel.encodeHorseName(id: horseData.horseId)
        let coordinateRegion = MKCoordinateRegion(center: newCoordinate, span: MKCoordinateSpan(latitudeDelta: 0.02, longitudeDelta: 0.02))
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func addSubviews() {
        mapView.addAnnotation(mapAnnotation)
        view.addSubview(mapView)
        constraint()
    }
    
    private func constraint() {
        mapView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        mapView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        mapView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
}

extension MapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let polygon = overlay as? MKPolygon {
            let renderer = MKPolygonRenderer(polygon: polygon)
            renderer.fillColor = Colors.tailDarkBeige.withAlphaComponent(0.25)
            renderer.strokeColor = Colors.tailBeige
            renderer.lineWidth = 2
            return renderer
        }
        return MKOverlayRenderer(overlay: overlay)
    }
}
