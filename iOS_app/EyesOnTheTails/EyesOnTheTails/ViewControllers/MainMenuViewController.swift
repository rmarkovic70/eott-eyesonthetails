//
//  MainMenuViewController.swift
//  EyesOnTheTails
//
//  Created by Apis IT on 21.04.2024..
//

import UIKit
import Lottie

class MainMenuViewController: UIViewController {

    var gotoMaps: () -> Void
    var gotoCredits: () -> Void
    
    init(gotoMaps: @escaping () -> Void, gotoCredits: @escaping () -> Void) {
        self.gotoMaps = gotoMaps
        self.gotoCredits = gotoCredits
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var animation: LottieAnimationView = {
        var lottie = LottieAnimationView(name: "horse_tailcolor")
        lottie.loopMode = .loop
        lottie.play()
        lottie.animationSpeed = 1.0
        lottie.translatesAutoresizingMaskIntoConstraints = false
        return lottie
    }()
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.backgroundColor = Colors.tailDarkGreen
        stack.layer.cornerRadius = 14
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var mapButton: UIButton = {
        let button = UIButton()
        button.setTitle("Map", for: .normal)
        button.titleLabel?.font = UIFont(name: "Teko-Light", size: 40)
        button.layer.cornerRadius = 6
        button.setTitleColor(Colors.tailBeige, for: .normal)
        button.setTitleColor(Colors.tailDarkBeige, for: .highlighted)
        button.addTarget(self, action: #selector(mapButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var creditsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Credits", for: .normal)
        button.titleLabel?.font = UIFont(name: "Teko-Light", size: 40)
        button.layer.cornerRadius = 6
        button.setTitleColor(Colors.tailBeige, for: .normal)
        button.setTitleColor(Colors.tailDarkBeige, for: .highlighted)
        button.addTarget(self, action: #selector(creditsButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var appTitle: UILabel = {
        let title = UILabel()
        title.text = "Eyes on the Tails"
        title.textAlignment = .center
        title.textColor = Colors.tailBeige
        title.font = UIFont(name: "Teko-Light", size: 40)
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        addSubviews()
    }
    
    private func setupUI() {
        view.backgroundColor = Colors.tailBlack
    }
    
    @objc private func mapButtonTapped() {
        gotoMaps()
    }
    
    @objc private func creditsButtonTapped() {
        gotoCredits()
    }

    private func addSubviews() {
        view.addSubview(appTitle)
        stackView.addArrangedSubview(animation)
        stackView.addArrangedSubview(mapButton)
        stackView.addArrangedSubview(creditsButton)
        view.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        stackView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        stackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        stackView.bottomAnchor.constraint(lessThanOrEqualTo: appTitle.topAnchor, constant: 0).isActive = true
        stackView.heightAnchor.constraint(lessThanOrEqualToConstant: 450).isActive = true

        appTitle.topAnchor.constraint(lessThanOrEqualTo: stackView.bottomAnchor, constant: 200).isActive = true
        appTitle.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        appTitle.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        appTitle.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
    }
}
